// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// dependencies
use bincode;
use libferroxid::conversion::endian;
use serde_derive::{Deserialize, Serialize};

// project
use crate::packets::{ArtNetObject, CliOption};
use crate::protocol::constants;
use crate::protocol::op_codes::OP_ADDRESS;

const CLI_OPTION: &str = "address";

pub struct ArtAddressObject {}

impl Default for ArtAddressObject {
    fn default() -> Self {
        ArtAddressObject {}
    }
}

#[derive(Serialize, Deserialize, Copy, Clone)]
#[repr(C, packed)]
struct ArtAddressPacket {
    id: [char; 8],
    op_code: [u8; 2],
    prot_ver_hi: u8,
    prot_ver_lo: u8,
}

impl CliOption for ArtAddressObject {
    fn cli_option() -> &'static str {
        CLI_OPTION
    }
}

impl ArtNetObject for ArtAddressObject {
    fn compose(&self) -> Vec<u8> {
        let packet = ArtAddressPacket {
            id: constants::ARTNET_PACKET_ID,
            op_code: endian::u16_to_little_endian_array(OP_ADDRESS),
            prot_ver_hi: constants::ARTNET_PROTOCOL_REVISION_NUMBER_HIGH_BYTE,
            prot_ver_lo: constants::ARTNET_PROTOCOL_REVISION_NUMBER_LOW_BYTE,
        };

        #[allow(clippy::needless_return)]
        return bincode::serialize(&packet).unwrap();
    }
}
