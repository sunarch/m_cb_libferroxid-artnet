// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

pub mod art_address;
pub mod art_command;
pub mod art_diag_data;
pub mod art_dmx;
pub mod art_input;
pub mod art_ip_prog;
pub mod art_ip_prog_reply;
pub mod art_nzs;
pub mod art_poll;
pub mod art_poll_reply;
pub mod art_sync;
pub mod art_time_code;
pub mod art_trigger;
pub mod art_vlc;

pub trait CliOption {
    fn cli_option() -> &'static str;
}

pub trait ArtNetObject {
    fn compose(&self) -> Vec<u8>;
}
