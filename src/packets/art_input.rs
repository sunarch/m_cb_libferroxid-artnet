// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// dependencies
use bincode;
use libferroxid::conversion::endian;
use serde_derive::{Deserialize, Serialize};

// project
use crate::packets::{ArtNetObject, CliOption};
use crate::protocol::constants;
use crate::protocol::op_codes::OP_INPUT;

const CLI_OPTION: &str = "input";

pub struct ArtInputObject {}

impl Default for ArtInputObject {
    fn default() -> Self {
        ArtInputObject {}
    }
}

#[derive(Serialize, Deserialize, Copy, Clone)]
#[repr(C, packed)]
struct ArtInputPacket {
    id: [char; 8],
    op_code: [u8; 2],
    prot_ver_hi: u8,
    prot_ver_lo: u8,
}

impl CliOption for ArtInputObject {
    fn cli_option() -> &'static str {
        CLI_OPTION
    }
}

impl ArtNetObject for ArtInputObject {
    fn compose(&self) -> Vec<u8> {
        let packet = ArtInputPacket {
            id: constants::ARTNET_PACKET_ID,
            op_code: endian::u16_to_little_endian_array(OP_INPUT),
            prot_ver_hi: constants::ARTNET_PROTOCOL_REVISION_NUMBER_HIGH_BYTE,
            prot_ver_lo: constants::ARTNET_PROTOCOL_REVISION_NUMBER_LOW_BYTE,
        };

        #[allow(clippy::needless_return)]
        return bincode::serialize(&packet).unwrap();
    }
}
