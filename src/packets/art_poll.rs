// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// dependencies
use bincode;
use libferroxid::conversion::endian;
use libferroxid::flags::bit_mask;
use serde_derive::{Deserialize, Serialize};

// project
use crate::packets::{ArtNetObject, CliOption};
use crate::protocol::constants;
use crate::protocol::diagnostic_priority::DiagPriority;
use crate::protocol::op_codes::OP_POLL;

const CLI_OPTION: &str = "poll";

pub struct ArtPollObject {
    pub enable_auto_updates: bool,

    pub enable_diagnostics: bool,
    pub disable_diagnostics_broadcast: bool,
    pub diag_priority: DiagPriority,

    pub disable_vlc_transmission: bool,

    pub enable_targeted_mode: bool,
    pub targeted_port_range_top: u16,
    pub targeted_port_range_bottom: u16,
}

impl Default for ArtPollObject {
    fn default() -> Self {
        ArtPollObject {
            enable_auto_updates: false,

            enable_diagnostics: false,
            disable_diagnostics_broadcast: false,
            diag_priority: Default::default(),

            disable_vlc_transmission: false,

            enable_targeted_mode: false,
            targeted_port_range_top: 0x0000,
            targeted_port_range_bottom: 0xFFFF,
        }
    }
}

#[derive(Serialize, Deserialize, Copy, Clone)]
#[repr(C, packed)]
struct ArtPollPacket {
    id: [char; 8],
    op_code: [u8; 2],
    prot_ver_hi: u8,
    prot_ver_lo: u8,
    flags: u8,
    diag_priority: u8,
    target_port_address_top_hi: u8,
    target_port_address_top_lo: u8,
    target_port_address_bottom_hi: u8,
    target_port_address_bottom_lo: u8,
}

impl CliOption for ArtPollObject {
    fn cli_option() -> &'static str {
        CLI_OPTION
    }
}

impl ArtPollObject {
    fn compose_flags(&self) -> u8 {
        let mut flags: u8 = 0b00000000;

        // Bits 7-6 : Unused, transmit as zero, do not test upon receipt.

        // Bit 5
        // 0 = Disable Targeted Mode.
        // 1 = Enable Targeted Mode.
        if self.enable_targeted_mode {
            flags |= bit_mask::BIT_MASK_5;
        }

        // Bit 4
        // 0 = Enable VLC transmission.
        // 1 = Disable VLC transmission.
        if self.disable_vlc_transmission {
            flags |= bit_mask::BIT_MASK_4;
        }

        // Bit 3
        // 0 = Diagnostics messages are broadcast. (if bit 2).
        // 1 = Diagnostics messages are unicast. (if bit 2).
        if self.disable_diagnostics_broadcast {
            flags |= bit_mask::BIT_MASK_3;
        }

        // Bit 2
        // 0 = Do not send me diagnostics messages.
        // 1 = Send me diagnostics messages.
        if self.enable_diagnostics {
            flags |= bit_mask::BIT_MASK_2;
        }

        // Bit 1
        // 0 = Only send ArtPollReply in response to an ArtPoll or ArtAddress.
        // 1 = Send ArtPollReply whenever Node conditions change.
        //     This selection allows the Controller to be informed of changes
        //         without the need to continuously poll.
        if self.enable_auto_updates {
            flags |= bit_mask::BIT_MASK_1;
        }

        // Bit 0 : Deprecated

        #[allow(clippy::needless_return)]
        return flags;
    }
}

impl ArtNetObject for ArtPollObject {
    fn compose(&self) -> Vec<u8> {
        let packet = ArtPollPacket {
            id: constants::ARTNET_PACKET_ID,
            op_code: endian::u16_to_little_endian_array(OP_POLL),
            prot_ver_hi: constants::ARTNET_PROTOCOL_REVISION_NUMBER_HIGH_BYTE,
            prot_ver_lo: constants::ARTNET_PROTOCOL_REVISION_NUMBER_LOW_BYTE,
            flags: self.compose_flags(),
            diag_priority: self.diag_priority.value(),
            target_port_address_top_hi: endian::u16_high_byte(self.targeted_port_range_top),
            target_port_address_top_lo: endian::u16_low_byte(self.targeted_port_range_top),
            target_port_address_bottom_hi: endian::u16_high_byte(self.targeted_port_range_bottom),
            target_port_address_bottom_lo: endian::u16_low_byte(self.targeted_port_range_bottom),
        };

        #[allow(clippy::needless_return)]
        return bincode::serialize(&packet).unwrap();
    }
}
