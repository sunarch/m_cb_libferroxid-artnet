// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

/// Diagnostics Priority codes
/// used in ArtPoll and ArtDiagData
#[derive(Default)]
pub enum DiagPriority {
    /// NOT STANDARD: Any message.
    #[default]
    DpAll,
    /// Low priority message.
    DpLow,
    /// Medium priority message.
    DpMed,
    /// High priority message.
    DpHigh,
    /// Critical priority message.
    DpCritical,
    /// Volatile message.
    /// Messages of this type are displayed on a single line
    /// in the DMX-Workshop diagnostics display.
    /// All other types are displayed in a list box.
    DpVolatile,
    /// NOT STANDARD: Unknown message.
    Unknown(u8),
}

impl DiagPriority {
    pub fn value(&self) -> u8 {
        match self {
            DiagPriority::DpAll => 0x00,
            DiagPriority::DpLow => 0x10,
            DiagPriority::DpMed => 0x40,
            DiagPriority::DpHigh => 0x80,
            DiagPriority::DpCritical => 0xe0,
            DiagPriority::DpVolatile => 0xf0,
            DiagPriority::Unknown(value) => *value,
        }
    }
}
