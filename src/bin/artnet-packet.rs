// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use std::net::UdpSocket;
use std::net::{Ipv4Addr, SocketAddrV4};

// project
use libferroxid_artnet::packets::art_poll::ArtPollObject;
use libferroxid_artnet::packets::{ArtNetObject, CliOption};
use libferroxid_artnet::protocol;

fn main() {
    let subcommand: String = match std::env::args().nth(1) {
        None => {
            println!("Subcommand missing!");
            return;
        }
        Some(value) => value,
    };
    let object: Box<dyn ArtNetObject> = match subcommand {
        option if { option == ArtPollObject::cli_option() } => Box::new(ArtPollObject::default()),
        _ => {
            println!("Unrecognized subcommand: {subcommand}");
            return;
        }
    };
    let packet: Vec<u8> = object.compose();

    let local_address: SocketAddrV4 = SocketAddrV4::new(
        Ipv4Addr::new(0, 0, 0, 0),
        protocol::constants::DEFAULT_IP_PORT,
    );

    let socket = UdpSocket::bind(local_address).expect("couldn't bind to address");
    socket
        .set_broadcast(true)
        .expect("failed to set socket broadcast");

    let remote_address: SocketAddrV4 = SocketAddrV4::new(
        Ipv4Addr::new(255, 255, 255, 255),
        protocol::constants::DEFAULT_IP_PORT,
    );

    println!("sending packet...");
    socket
        .send_to(&packet, remote_address)
        .expect("Packet could not be sent");
}
